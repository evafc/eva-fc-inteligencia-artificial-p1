# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""
import util
import searchAgents

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    """

    """ print("Start:", problem.getStartState())
     print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
     print("Start's successors:", problem.getSuccessors(problem.getStartState())) """
    
    "***MY CODE HERE ***"

    # Initialice the nodes and necesary variables
    
    frontier = util.Stack()
    start = (problem.getStartState(), [])
    frontier.push(start)
    
    expanded = [] 
    
    while not frontier.isEmpty():

        # extract the actual node (LIFO)
        node, path_to_goal = frontier.pop()
        
        if problem.isGoalState(node):
            return path_to_goal

        # for not re-expand nodes

        if node not in expanded:
            expanded.append(node)

            sons = problem.getSuccessors(node)

            for element in sons:
                if element[0] not in expanded:
                    # update the frontier, and the path, getting the direction
                    # section of the actual node's succesor 
                    
                    frontier.push((element[0], path_to_goal + [element[1]]))
    return [] 
    
def breadthFirstSearch(problem: SearchProblem):
    """Search the shallowest nodes in the search tree first."""
    "*** MY CODE HERE ***"
    
    # Initialice the nodes and necesary variables
    
    frontier = util.Queue()
    start = (problem.getStartState(), [])
    frontier.push(start)
    
    expanded = [] 

    while not frontier.isEmpty():
        
        # extract the actual node (FIFO)
        node, path_to_goal = frontier.pop()
        
        if problem.isGoalState(node):
            return path_to_goal

        # for not re-expand nodes

        if node not in expanded:
            expanded.append(node)

            sons = problem.getSuccessors(node)

            for element in sons:
                if element[0] not in expanded:
                    # update the frontier, and the path, getting the direction
                    # section of the actual node's succesor 
                    
                    frontier.push((element[0], path_to_goal + [element[1]]))
    return [] 

def uniformCostSearch(problem: SearchProblem):
    """Search the node of least total cost first."""
    "*** MY CODE HERE ***"
    
    # Initialice the nodes and necesary variables

    frontier = util.PriorityQueue()
    start = (problem.getStartState(), [], 0)
    
    priority = 0
    frontier.push(start, priority)
    
    expanded = [] 

    while not frontier.isEmpty():

        # extract the actual node (FIFO) with it's priority

        node, path_to_goal, priority = frontier.pop()
        
        if problem.isGoalState(node):
            return path_to_goal
            
        # for not re-expand nodes

        if node not in expanded:
            expanded.append(node)

            sons = problem.getSuccessors(node)

            for element in sons:
                if element[0] not in expanded:
                    # update the frontier, and the path, getting the direction
                    # section of the actual node's succesor and it's priority
                    
                    new_priority = priority + element[2]
                    frontier.push((element[0], path_to_goal + [element[1]], new_priority), new_priority)
    return []

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** MY CODE HERE ***"

     
    # Initialice the nodes and necesary variables
    
    frontier = util.PriorityQueue()
    start = (problem.getStartState(), [], 0)
   
    priority = 0
    frontier.push(start, heuristic(problem.getStartState(), problem))
    
    expanded = [] 

    while not frontier.isEmpty():
        
        # extract the actual node (FIFO) with it's priority
        
        node, path_to_goal, priority = frontier.pop()
        
        if problem.isGoalState(node):
            return path_to_goal

        # for not re-expand nodes    
        
        if node not in expanded:
            expanded.append(node)

            sons = problem.getSuccessors(node)

            for element in sons:
                if element[0] not in expanded:
                    # update the frontier, and the path, getting the direction
                    # section of the actual node's succesor and it's priority, adding the heuristic attached to it 
                    new_priority = priority + element[2]
                    frontier.push((element[0], path_to_goal + [element[1]], new_priority), new_priority + heuristic(element[0], problem))
    return []
   
# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
